package api

import (
	"fmt"
	
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/standard"
	"github.com/labstack/echo/middleware"

	"nskwater-api/config"
	"nskwater-api/store"
	"net/http"
)

const (
	storeContextKey  = "store"
	configContextKey = "config"
)

// Env contains any externally configurable objects required by the api
type Env struct {
	Store  store.Store
	*config.Config
}

// Run runs the API server
func Run(env *Env) error {

	router := echo.New()
	router.SetDebug(true)
	router.Use(middleware.Logger())
	router.Use(middleware.Recover())
	router.Use(middleware.CORSWithConfig(middleware.DefaultCORSConfig))
	//	e.Use(middleware.CSRF())

	router.Use(func(h echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Set(storeContextKey, env.Store)
			c.Set(configContextKey, env.Config)
			return h(c)
		}
	})

	//	routes
	router.Get("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "api")
	})

	// API
	api := router.Group("/api")

	// authentication
	auth := api.Group("/auth")
	auth.Post("/login", login)
	
	// private
	private := api.Group("/private")
	private.Use(middleware.JWT([]byte(env.Config.SecretKey)))

	token := private.Group("/token")
	token.Get("", getTokenInfo)
	
	// ports
	ports := private.Group("/ports")
	ports.Get("", getPorts)
	
	router.Run(standard.New(fmt.Sprintf(":%v", env.Port)))
	
	return nil
}

type jwtCustomClaims struct {
	Name	string	`json:"name"`
	jwt.StandardClaims
}

func getStore(c echo.Context) store.Store {
	return c.Get(storeContextKey).(store.Store)
}

func getConfig(c echo.Context) *config.Config {
	return c.Get(configContextKey).(*config.Config)
}

package api

import (
	"net/http"
	"time"

	"github.com/labstack/echo"
	"github.com/dgrijalva/jwt-go"

	"nskwater-api/models"
)

func login(c echo.Context) error {
	
	var (
		identifier = c.FormValue("identifier")
		password = c.FormValue("password")
		config = getConfig(c)
		store = getStore(c)
		conn = store.Conn()
	)
	
	user := &models.User{}

	if  err := store.Users().GetByNameOrEmail(conn, user, identifier); err != nil || !user.CheckPassword(password) {
		return echo.ErrUnauthorized
	}

	claims := jwt.MapClaims{}
	claims["name"] = user.Name
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()
	tokenWithClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	
	resultToken, err := tokenWithClaims.SignedString([]byte(config.SecretKey))
	if err != nil {
		return err
	}
	
	return c.JSON(http.StatusOK, map[string]string{
		"token": resultToken,
		"username": user.Name,
	})
}

func getTokenInfo(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)

	return c.JSON(http.StatusOK, map[string]string{
		"token": user.Raw,
		"username": claims["name"].(string),
	})
}
package api

import (
	"net/http"
	"nskwater-api/models"
	"github.com/labstack/echo"
)

func getPorts(c echo.Context) error {
	
	var (
		s = getStore(c)
		ports = []models.Port{}
	)
	
	if err := s.Ports().GetAll(s.Conn(), &ports); err != nil {
		return err
	}
	
	return c.JSON(http.StatusOK, ports)
}
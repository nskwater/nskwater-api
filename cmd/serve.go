package cmd

import (
	"nskwater-api/api"
	"nskwater-api/config"
	"nskwater-api/store"
)

// Serve runs the webserver
func Serve(cfg *config.Config) error {
	appStore, err := store.New(cfg)
	if err != nil {
		return err
	}

	defer appStore.Close()

	env := &api.Env{
		Store:  appStore,
		Config: cfg,
	}

	return api.Run(env)
}

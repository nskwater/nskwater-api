package config

import (
	"encoding/base64"
	"errors"
	"strconv"
	
	"github.com/gorilla/securecookie"
	"github.com/joho/godotenv"
)

const (
	configPort = "PORT"
	configDbUrl = "DB_URL"
	configDbMaxConnections = "DB_MAX_CONNECTIONS"
	configDbMigrationsDir = "DB_MIGRATIONS_DIR"

	defaultPort            = 5000
	defaultDbMaxConnections = 99
	defaultDbMigrationsDir = "./migrations"
)

var (
	ErrMissingDatabaseURL = errors.New("Database URL is missing")
)

// Default returns a ready-made configuration with sensible settings
func Load() (*Config, error) {
	var env map[string]string
	env, err := godotenv.Read()
	if (err != nil) {
		return nil, err
	}
	
	dbUrl := env[configDbUrl]
	if (dbUrl == "") {
		return nil, ErrMissingDatabaseURL
	}

	port, err := strconv.ParseInt(env[configPort], 10, 32)
	if (err != nil) {
		port = defaultPort
	}
	
	dbMaxConnections, err := strconv.ParseInt(env[configDbMaxConnections], 10, 32)
	if (err != nil) {
		dbMaxConnections = defaultDbMaxConnections;
	}
	
	dbMigrationsDir := env[configDbMigrationsDir]
	if (dbMigrationsDir == "") {
		dbMigrationsDir = defaultDbMigrationsDir
	}
	
	return &Config{
		Port: int(port),
		DbUrl: dbUrl,
		DbMaxConnections: int(dbMaxConnections),
		DbMigrationsDir: dbMigrationsDir,
		SecretKey: RandomKey(),
	}, nil
}

// Config is server configuration
type Config struct {
	Port             int
	DbUrl            string
	DbMaxConnections int
	DbMigrationsDir  string
	SecretKey        string
}

// RandomKey generates a printable, random 32 byte string
func RandomKey() string {
	return base64.StdEncoding.EncodeToString(securecookie.GenerateRandomKey(32))
}

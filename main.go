package main

import (
	"os"

	"nskwater-api/cmd"
	"nskwater-api/config"

	"github.com/codegangsta/cli"
)

func main() {
	app := cli.NewApp()
	app.EnableBashCompletion = true

	app.Commands = []cli.Command{
		{
			Name:  "serve",
			Usage: "Run the server",
			Action: func(c *cli.Context) error {
				cfg, err := config.Load()
				if (err != nil) {
					return cli.NewExitError(err.Error(), 1)
				}
				err = cmd.Serve(cfg)
				if (err != nil) {
					return cli.NewExitError(err.Error(), 2)
				}
				return nil
			},
		},
	}

	app.Run(os.Args)
}

CREATE SEQUENCE users_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE users (
  id integer NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  name text NOT NULL,
  email text NOT NULL,
  password text NOT NULL,
  created_at timestamp with time zone DEFAULT now(),
  CONSTRAINT users_pkey PRIMARY KEY (id),
  CONSTRAINT users_email_key UNIQUE (email),
  CONSTRAINT users_name_key UNIQUE (name)
);

INSERT INTO users(name, password, email)
    VALUES ('orion', '$2a$10$d2296vRXlZdzXGRvD7vcc.VWbVfWffA53V6UkUbpLUiE/.uRvZmgC', '');

CREATE TABLE ports (
  id text NOT NULL,
  idm text,
  tech_nomer text,
  indentory_id text,
  str_id text,
  num_house text,
  xs text,
  xt text,
  z text,
  p1 text,
  p2 text,
  p3 text,
  q1 text,
  q2 text,
  q3 text,
  note text,
  date_vvoda text,
  owner text,
  "user" text,
  raspolog text,
  material text,
  depth real,
  prisposoblenie text,
  gaz text,
  voda text,
  razmer_plan text,
  region_id text,
  coordinates text,
  created_at timestamp with time zone DEFAULT now(),
  CONSTRAINT "Ports_pkey" PRIMARY KEY (id)
);
package models

type Port struct {
	Id			string	`db:"id" json:"id"`
	TechNomer	string	`db:"tech_nomer" json:"tech_nomer"`
	Coordinates	string	`db:"coordinates" json:"coordinates"`
	Street		string	`db:"str_id" json:"street"`
	NumHouse	string	`db:"num_house" json:"num_house"`
	Raspolog	string	`db:"raspolog" json:"raspolog"`
	Note		string	`db:"note" json:"note"`
}
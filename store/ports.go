package store

import (
	"nskwater-api/models"
	"github.com/jmoiron/sqlx"
)

type PortReader interface {
	GetAll(DataHandler, *[]models.Port) error
}

//type PortWriter interface {
//}

type PortStore interface {
	PortReader
//	PortWriter
}

type PortSqlStore struct {
	PortReader
//	PortWriter
}

func newPortStore() PortStore {
	return &PortSqlStore{
		PortReader: &portSqlReader{},
//		PortWriter: &portSqlWriter{},
	}
}

type portSqlReader struct{}
//type portSqlWriter struct{}

func (r *portSqlReader) GetAll(dh DataHandler, ports *[]models.Port) error {
	q := "SELECT id, tech_nomer, coordinates, str_id, num_house, raspolog, note FROM ports"
	return handleError(sqlx.Select(dh, ports, q), q)
}

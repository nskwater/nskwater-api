package store

import (
	"nskwater-api/config"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/mattes/migrate/migrate"
	_ "github.com/mattes/migrate/driver/postgres"
	"github.com/labstack/gommon/log"
)

type closer interface {
	Close() error
}

type beginner interface {
	Begin() (Transaction, error)
}

type preparer interface {
	Preparex(string) (*sqlx.Stmt, error)
	PrepareNamed(string) (*sqlx.NamedStmt, error)
}

// DataHandler is a connection or transaction executing queries
type DataHandler interface {
	sqlx.Ext
	preparer
}

// Connection is a connection to the data store
type Connection interface {
	DataHandler
	beginner
	closer
}

// Transaction is a data store transaction
type Transaction interface {
	DataHandler
	Rollback() error
	Commit() error
}

// Store manages all data store interactions for the application
type Store interface {
	Close() error
	Conn() Connection
	Ports () PortStore
	Users() UserStore
}

type sqlStore struct {
	conn  Connection
	users UserStore
	ports PortStore
}

type sqlConnection struct {
	*sqlx.DB
}

func (store *sqlStore) Close() error {
	return store.conn.Close()
}

func (store *sqlStore) Conn() Connection {
	return store.conn
}

func (conn *sqlConnection) Close() error {
	return conn.DB.Close()
}

func (conn *sqlConnection) Begin() (Transaction, error) {
	tx, err := conn.DB.Beginx()
	if err != nil {
		return nil, err
	}
	return &sqlTransaction{tx}, nil
}

type sqlTransaction struct {
	*sqlx.Tx
}

func (store *sqlStore) Users() UserStore {
	return store.users
}

func (store *sqlStore) Ports() PortStore {
	return store.ports
}

func newSQLStore(db *sqlx.DB) Store {
	return &sqlStore{
		conn:  &sqlConnection{db},
		users: newUserStore(),
		ports: newPortStore(),
	}
}

// New returns a new Store instance
func New(cfg *config.Config) (Store, error) {
	dbVersion, err := migrate.Version(cfg.DbUrl, cfg.DbMigrationsDir)
	log.Info("Database version: ", dbVersion)
	
	db, err := sqlx.Connect("postgres", cfg.DbUrl)
	if err != nil {
		return nil, err
	}
	db.SetMaxOpenConns(cfg.DbMaxConnections)
	return newSQLStore(db), nil
}
